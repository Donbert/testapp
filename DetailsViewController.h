//
//  DetailsViewController.h
//  TestApp
//
//  Created by Donald Bertamini on 9/17/15.
//  Copyright (c) 2015 Cloudy Coast Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductClass.h"

@interface DetailsViewController : UIViewController

@property (nonatomic, strong) ProductClass *currentProduct;
@property (nonatomic, strong) UIImage *currentImage;

@end
