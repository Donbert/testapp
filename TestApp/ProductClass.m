//
//  ProductClass.m
//  TestApp
//
//  Created by Donald Bertamini on 9/16/15.
//  Copyright (c) 2015 Cloudy Coast Software. All rights reserved.
//

#import "ProductClass.h"

@implementation ProductClass
@synthesize prodDesc;
@synthesize prodName;
@synthesize prodPrice;
@synthesize prodImageURL;
@synthesize prodLat;
@synthesize prodLon;
@end
