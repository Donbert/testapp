//
//  ProductCollectionViewCell.m
//  TestApp
//
//  Created by Donald Bertamini on 9/17/15.
//  Copyright (c) 2015 Cloudy Coast Software. All rights reserved.
//

#import "ProductCollectionViewCell.h"

@implementation ProductCollectionViewCell
@synthesize priceLabel;

- (instancetype)initWithCoder:(NSCoder *)coder
{
   self = [super initWithCoder:coder];
   if (self) {
      
      self.layer.cornerRadius = 3.0;
      self.clipsToBounds = YES;
      self.layer.borderWidth = 1.0;
      self.layer.borderColor = [[UIColor clearColor]CGColor];
   }
   return self;
}

- (void)prepareForReuse {
}


@end
