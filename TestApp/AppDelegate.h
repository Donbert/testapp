//
//  AppDelegate.h
//  TestApp
//
//  Created by Donald Bertamini on 9/16/15.
//  Copyright (c) 2015 Cloudy Coast Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

