//
//  ProductClass.h
//  TestApp
//
//  Created by Donald Bertamini on 9/16/15.
//  Copyright (c) 2015 Cloudy Coast Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductClass : NSObject

@property (nonatomic, strong) NSString *prodName;
@property (nonatomic, strong) NSString *prodDesc;
@property (nonatomic, strong) NSString *prodPrice;
@property (nonatomic, strong) NSString *prodImageURL;
@property (nonatomic, strong) NSString *prodLat;
@property (nonatomic, strong) NSString *prodLon;


@end
