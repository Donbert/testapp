//
//  ViewController.m
//  TestApp
//
//  Created by Donald Bertamini on 9/16/15.
//  Copyright (c) 2015 Cloudy Coast Software. All rights reserved.
//  https://core.antsquare.com/products/trending

#define kAntSquareURL @"https://core.antsquare.com/products/trending"

#import "ViewController.h"
#import <AFNetworking.h>
#import <UIImageView+AFNetworking.h>
#import "ProductClass.h"
#import "ProductCollectionViewCell.h"
#import "DetailsViewController.h"

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;
@property (nonatomic, strong) NSMutableArray *collectionArray;
@end

@implementation ViewController
@synthesize collectionArray;
@synthesize myCollectionView;


#pragma mark - View Contoller Life Cycle
- (void)viewDidLoad {
   [super viewDidLoad];
   [self retrieveData];
}

-(BOOL)prefersStatusBarHidden{
   return YES;
}

#pragma mark - Retrieve and Parse Data
// use AFNetworking to retrieve trending data
-(void)retrieveData{
   AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
   [manager GET:kAntSquareURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
      //if we are successful create a colletionView array
      [self parseData:responseObject];
   } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
      NSLog(@"Error: %@", error);
      UIAlertController *errorAlert = [UIAlertController
                                       alertControllerWithTitle:@"Alert"
                                       message:@"You require a connection..."
                                       preferredStyle:UIAlertControllerStyleAlert];
      
      UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * action){
                     [self.navigationController popViewControllerAnimated:YES]; }];
      
      [errorAlert addAction:ok];
      [self presentViewController:errorAlert animated:YES completion:nil];
   }];
}

-(void)parseData:(NSDictionary *)theDictionary{
   
   // create the product array to parse
   NSArray *productArray = [theDictionary objectForKey:@"products"];
   
   // start a new array for the collectionview
   collectionArray = [NSMutableArray array];
   // loop through the array of products from server
   for(NSDictionary *prodDict in productArray){
      
      NSArray *imageArray = [prodDict objectForKey:@"images"];
      
      // if we have an image, add a record for the collection view array
      // just show the first image per product in the collectionView
      
      if(imageArray.count > 0){
         // new element for the array
         ProductClass *productRecord = [[ProductClass alloc]init];
         [productRecord setProdPrice:[prodDict objectForKey:@"price"]];
         [productRecord setProdName:[prodDict objectForKey:@"name"]];
         [productRecord setProdDesc:[prodDict objectForKey:@"description"]];
         [productRecord setProdLat:[prodDict objectForKey:@"lat"]];
         [productRecord setProdLon:[prodDict objectForKey:@"lon"]];
         [productRecord setProdImageURL:[imageArray objectAtIndex:0]];
         [collectionArray addObject:productRecord];
      }
   }
   [myCollectionView reloadData];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
   
   if ([segue.identifier isEqualToString:@"toDetails"]){
      DetailsViewController *detailsVC = (DetailsViewController *)[segue destinationViewController];
      
      // pass in a product object
      NSIndexPath *indexPath = [[myCollectionView indexPathsForSelectedItems] lastObject];
      detailsVC.currentProduct = [collectionArray objectAtIndex:indexPath.row];
      
      // pass in the image
      ProductCollectionViewCell *collectionCell = (ProductCollectionViewCell *)[myCollectionView cellForItemAtIndexPath:indexPath];
      detailsVC.currentImage = collectionCell.prodImageView.image;
   }
}

#pragma mark - Collection View Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
   return collectionArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
   
   ProductCollectionViewCell *cell = (ProductCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];

   // use AFNetworking to set the product image
   [cell.prodImageView setImageWithURL:[NSURL URLWithString:[[collectionArray objectAtIndex:indexPath.row]prodImageURL]]];
   
   //format the price string to currency
   cell.priceLabel.text = [self formatCurrencyWithString:[NSString stringWithFormat:@"%@", [[collectionArray objectAtIndex:indexPath.row]prodPrice]]];
   cell.priceLabel.textColor = [UIColor colorWithRed:0.556863F green:0.215686F blue:1.000000F alpha:1.0F];
   
   // show the product name in the cells label
   cell.nameLabel.text = [[collectionArray objectAtIndex:indexPath.row]prodName];

   return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   // transition to the details VC
   [self performSegueWithIdentifier:@"toDetails" sender:self];
   
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
   
   // Adjust cell size for orientation and device screen size   
   float myWidth = myCollectionView.frame.size.width/2.14;
   
   if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
      return CGSizeMake(myWidth/1.5, myWidth/1.5); // smaller cell for landscape
   }
   return CGSizeMake(myWidth, myWidth);
}

// smoother transition of collectioView in orientation changes
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
   [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
   
   [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context){
       [myCollectionView performBatchUpdates:nil completion:nil];
    }  completion:nil];
}

#pragma mark - Format for currency
- (NSString *)formatCurrencyWithString: (NSString *) string{
  
   NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
   [currencyStyle setFormatterBehavior:NSNumberFormatterBehavior10_4];
   [currencyStyle setNumberStyle:NSNumberFormatterNoStyle];
   
   //create number from string
   NSNumber * balance = [currencyStyle numberFromString:string];
   
   //now set to currency format
   [currencyStyle setNumberStyle:NSNumberFormatterCurrencyStyle];
   
   // get formatted string
   NSString* formatted = [currencyStyle stringFromNumber:balance];
   
   return formatted;
}

@end
