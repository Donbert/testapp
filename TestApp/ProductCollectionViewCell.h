//
//  ProductCollectionViewCell.h
//  TestApp
//
//  Created by Donald Bertamini on 9/17/15.
//  Copyright (c) 2015 Cloudy Coast Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *prodImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;


@end
