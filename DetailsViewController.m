//
//  DetailsViewController.m
//  TestApp
//
//  Created by Donald Bertamini on 9/17/15.
//  Copyright (c) 2015 Cloudy Coast Software. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UITextView *mainTextView;
@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation DetailsViewController
@synthesize currentProduct;
@synthesize mainTextView;
@synthesize mainImageView;
@synthesize currentImage;
@synthesize nameLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
   [super viewWillAppear:animated];

   mainTextView.text = currentProduct.prodDesc;
   mainImageView.image = currentImage;
   nameLabel.text = currentProduct.prodName;
}

- (void)viewDidLayoutSubviews {
   [mainTextView setContentOffset:CGPointZero animated:NO];
}

- (IBAction)closeClick:(UIButton *)sender {
   [self dismissViewControllerAnimated:YES completion:^{
      // test
   }];
}


@end
